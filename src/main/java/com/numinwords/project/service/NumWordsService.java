package com.numinwords.project.service;

import java.util.HashMap;
import java.util.Map;

import com.numinwords.project.api.INumberSystem;
import com.numinwords.project.convert.ConvertNumberToEnWords;
import com.numinwords.project.convert.ConvertNumberToRuWords;
import com.numinwords.project.model.NumWords;
import com.numinwords.project.model.Words;
import com.numinwords.utils.Convert;

public class NumWordsService {

	private static final short NUM_LANG = 2;

	static public Words getWordsfromNumber(long number) {
		String numRuDec = ConvertNumberToRuWords.convertToWords(number);
		String numRuOct = ConvertNumberToRuWords.convertToWords(Convert.getOctal(number));

		String numEnDec = ConvertNumberToEnWords.convertToWords(number);
		String numEnOct = ConvertNumberToEnWords.convertToWords(Convert.getOctal(number));

		String[] arr = { numRuDec, numRuOct, numEnDec, numEnOct };
		return fillData(arr);
	}

	// Fill Words data use array
	static private Words fillData(String[] arr) {

		Map<String, String> en_words = new HashMap<>(NUM_LANG);
		Map<String, String> ru_words = new HashMap<>(NUM_LANG);
		ru_words.put(INumberSystem.DECIMAL, arr[0]);
		ru_words.put(INumberSystem.OCTAL, arr[1]);
		en_words.put(INumberSystem.DECIMAL, arr[2]);
		en_words.put(INumberSystem.OCTAL, arr[3]);
		NumWords res = new NumWords();
		res.setEn(en_words);
		res.setRu(ru_words);
		return (new Words(res));
	}

}
