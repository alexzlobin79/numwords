package com.numinwords.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumWordsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumWordsApplication.class, args);
	}
}
