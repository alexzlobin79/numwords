package com.numinwords.project.model;

import java.util.Map;

public class NumWords {

	private Map<String, String> en;

	private Map<String, String> ru;

	public Map<String, String> getEn() {
		return en;
	}

	public void setEn(Map<String, String> en) {
		this.en = en;
	}

	public Map<String, String> getRu() {
		return ru;
	}

	public void setRu(Map<String, String> ru) {
		this.ru = ru;
	}

}
