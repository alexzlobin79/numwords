package com.numinwords.project.convert;

public enum FormNoun {

	SINGULAR_NOMINATIVE, SINGULAR_GENITIVE, PLURAL_NOMINATIVE

}
