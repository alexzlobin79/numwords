package com.numinwords.project.convert;

import com.numinwords.utils.Convert;

public class ConvertNumberToEnWords {

	private static final String[] tensNames = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
			"eighty", "ninety" };
	// Define digits
	private static final String[] namesFirstSecondTen = { "", "one", "two", "three", "four", "five", "six", "seven",
			"eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
			"eighteen", "nineteen" };
	private static final String nameHundred = "hundred";
	private static final String nameThousand = "thousand";
	private static final String nameMillion = "million";
	private static final String nameBillion = "billion";
	private static final String nameTrillion = "trillion";
	private static final String DELIMITER = " ";

	private ConvertNumberToEnWords() {
	}

	private static String convertLessThanOneThousand(short number) {

		if (number < 0 || number > 999)
			throw new IllegalArgumentException("Number out of range(0-999)");

		StringBuilder result = new StringBuilder(100);

		int[] digits = Convert.getDigitsNumberThreeDigit(number);

		if (digits[1] < 2) {

			// add word of numbers where ten=1 or 0

			result.append(DELIMITER).append(namesFirstSecondTen[digits[1] * Convert.TEN + digits[2]]);

		} else {

			// add tens

			result.append(DELIMITER).append(tensNames[digits[1]]);

			// add hyphen if it is need
			if (digits[2] != 0) {
				result.append("-");
			} else {
				result.append(DELIMITER);
			}
			;

			// add units
			result.append(namesFirstSecondTen[digits[2]]);

		}
		// if no hundreds
		if (digits[0] == 0)
			return result.toString().trim();
		// combine all
		return (result.insert(0, nameHundred).insert(0, DELIMITER).insert(0, namesFirstSecondTen[digits[0]])).toString()
				.trim();
	}

	// Convert to words
	public static String convertToWords(long number)

	{
		StringBuilder result = new StringBuilder(1000);

		if (number == 0)
			return "zero";

		long number_positive = number;
		if (number < 0) {
			number_positive = -number;
			result.append("minus ");
		}

		short groups[] = Convert.decompNumberInGroupThreeDigit(number_positive);

		for (int i = 0; i < 5; i++) {
			if (groups[i] > 0) {

				if (i == 4) {
					result.append(convertLessThanOneThousand(groups[i]));
				}
				if (i == 3) {
					result.append(convertLessThanOneThousand(groups[i])).append(DELIMITER).append(nameThousand);
				}
				if (i == 2) {
					result.append(convertLessThanOneThousand(groups[i])).append(DELIMITER).append(nameMillion);
				}
				if (i == 1) {
					result.append(convertLessThanOneThousand(groups[i])).append(DELIMITER).append(nameBillion);
				}
				if (i == 0) {
					result.append(convertLessThanOneThousand(groups[i])).append(DELIMITER).append(nameTrillion);
				}
				result.append(DELIMITER);
			}

		}

		return result.toString().trim();
	}

}
