package com.numinwords.project.convert;

import com.numinwords.utils.Convert;

public class ConvertNumberToRuWords {
	// Define digits in female/male

	private static final String namesFirstTen[][] = {
			{ "одна", "две", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять" }, { "один", "два" } };
	private static final String namesSecondTen[] = { "десять", "одиннадцать", "двенадцать", "тринадцать",
			"четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" };
	private static final String namesOtherTens[] = { "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят",
			"семьдесят", "восемьдесят", "девяносто" };
	private static final String namesHundreds[] = { "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот",
			"семьсот", "восемьсот", "девятьсот" };
	private static final String nameThousand[] = { "тысяча", "тысячи", "тысяч" };
	private static final String nameMillion[] = { "миллион", "миллиона", "миллионов" };
	private static final String nameBillion[] = { "миллиард", "миллиарда", "миллиардов" };
	private static final String nameTrillion[] = { "триллион", "триллиона", "триллионов" };
	private static final String DELIMITER = " ";

	// Function convert numbers (in range from 0 to 999) in words
	private static String convertLessThanOneThousand(short number, Gender gender)

	{

		if (number < 0 || number > 999)
			throw new IllegalArgumentException("Number out of range(0-999)");

		// if (number == 0)
		// return "ноль";

		StringBuilder result = new StringBuilder(100);

		int[] digits = Convert.getDigitsNumberThreeDigit(number);

		// add word of hundreds

		if (digits[0] != 0)
			result.append(DELIMITER).append(namesHundreds[digits[0] - 1]);

		// add word of decimal

		switch (digits[1]) {
		case 0:
			break;
		case 1: {
			result.append(DELIMITER).append(namesSecondTen[digits[2]]);
			return result.toString().trim();
		}
		default:
			result.append(DELIMITER).append(namesOtherTens[digits[1] - 2]);
		}

		// add word of units
		switch (digits[2]) {
		case 0:
			break;
		case 1:
			result.append(DELIMITER).append(namesFirstTen[gender.ordinal()][0]);
			break;
		case 2:
			result.append(DELIMITER).append(namesFirstTen[gender.ordinal()][1]);
			break;
		default:
			result.append(DELIMITER).append(namesFirstTen[0][digits[2] - 1]);
		}

		return result.toString().trim();
	}
	// get form noun,for example PLURAL_NOMINATIVE
	// (миллионов,тысяч,миллиардов),SINGULAR_GENITIVE(миллиона,миллиарда) и
	// SINGULAR_NOMINATIVE (миллион,миллиард)

	private static FormNoun getFormNounNumberThreeDigit(short number) {

		int[] digits = Convert.getDigitsNumberThreeDigit(number);
		if (digits[1] == 1)
			return FormNoun.PLURAL_NOMINATIVE;
		if (digits[2] == 1)
			return FormNoun.SINGULAR_NOMINATIVE;
		if (digits[2] > 1 && digits[2] < 5)
			return FormNoun.SINGULAR_GENITIVE;
		return FormNoun.PLURAL_NOMINATIVE;
	}

	// Convert to words
	public static String convertToWords(long number)

	{
		StringBuilder result = new StringBuilder(1000);

		if (number == 0)
			return "ноль";

		long number_positive = number;
		if (number < 0) {
			number_positive = -number;
			result.append("минус ");
		}

		short groups[] = Convert.decompNumberInGroupThreeDigit(number_positive);

		for (int i = 0; i < 5; i++) {
			if (groups[i] > 0) {
				FormNoun formNounNumber = getFormNounNumberThreeDigit(groups[i]);

				if (i == 4) {
					result.append(convertLessThanOneThousand(groups[i], Gender.MALE));
				}
				if (i == 3) {
					result.append(convertLessThanOneThousand(groups[i], Gender.FEMALE)).append(DELIMITER)
							.append(nameThousand[formNounNumber.ordinal()]);
				}
				if (i == 2) {
					result.append(convertLessThanOneThousand(groups[i], Gender.MALE)).append(DELIMITER)
							.append(nameMillion[formNounNumber.ordinal()]);
				}
				if (i == 1) {
					result.append(convertLessThanOneThousand(groups[i], Gender.MALE)).append(DELIMITER)
							.append(nameBillion[formNounNumber.ordinal()]);
				}
				if (i == 0) {
					result.append(convertLessThanOneThousand(groups[i], Gender.MALE)).append(DELIMITER)
							.append(nameTrillion[formNounNumber.ordinal()]);
				}
				result.append(DELIMITER);
			}

		}

		return result.toString().trim();
	}

}
