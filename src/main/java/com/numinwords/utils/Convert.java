package com.numinwords.utils;

public class Convert {

	// function convert decimal to octal

	public static long getOctal(long number) {

		return Long.valueOf(Long.toOctalString(Math.abs(number)));
	}

	public static  final int TEN = 10;
	public static  final int HUNDRED = 100;
	public static  final int THOUSAND = 1000;
	public static final long TRILLION = (long) 1E12;

	// decomposition of a number into groups of max three-digit numbers , 1 123 324
	// => [0,0,0,123,324];105 123 324 => [0,0,105,123,324]

	public static short[] decompNumberInGroupThreeDigit(long number) {
		long digit = TRILLION;// start from trillion
		long number1 = number;
		short[] res_decomposition = new short[5];
		for (byte i = 0; i < 5; i++) {
			res_decomposition[i] = (short) (number1 / digit);
			number1 = number1 - res_decomposition[i] * digit;
			digit = digit / THOUSAND;
		}
		return res_decomposition;
	}

	public static int[] getDigitsNumberThreeDigit(int number) {

		int hundreds = number / HUNDRED;// get numbers of hundreds
		int remNumHundred = number % HUNDRED;// get number minus hundreds
		int decimals = remNumHundred / TEN;// get numbers of decimals
		int units = remNumHundred % TEN;// get numbers of units
		int[] digits = { hundreds, decimals, units };
		return digits;
	}

}
