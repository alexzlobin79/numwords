package com.numinwords.project;

import static org.junit.Assert.*;
import org.junit.Test;
import com.numinwords.project.convert.ConvertNumberToRuWords;
import com.numinwords.utils.Convert;

public class TestConvertNumberInRuWords {

	@Test
	public void TestConvertOneThreeDigits() {

		int[] source = { 0, 1, 9, 19, 22, 40, 101, 119, 145, 100, 940 };
		String[] expected = { "ноль", "один", "девять", "девятнадцать", "двадцать два", "сорок", "сто один",
				"сто девятнадцать", "сто сорок пять", "сто", "девятьсот сорок" };
		for (int i = 0; i < source.length; i++) {
			assertEquals(ConvertNumberToRuWords.convertToWords(source[i]), expected[i]);
		}
	}

	@Test
	public void TestConvertFourFiveDigits() {

		int[] source = { 1101, 2119, 5121, 6452, 50000 };
		String[] expected = { "одна тысяча сто один", "две тысячи сто девятнадцать", "пять тысяч сто двадцать один",
				"шесть тысяч четыреста пятьдесят два", "пятьдесят тысяч" };
		for (int i = 0; i < source.length; i++) {
			assertEquals(ConvertNumberToRuWords.convertToWords(source[i]), expected[i]);
		}
	}

	@Test
	public void TestConvertBigNumber() {

		long[] source = { 55451129673430L };
		String[] expected = {
				"пятьдесят пять триллионов четыреста пятьдесят один миллиард сто двадцать девять миллионов шестьсот семьдесят три тысячи четыреста тридцать" };
		for (int i = 0; i < source.length; i++) {
			assertEquals(ConvertNumberToRuWords.convertToWords(source[i]), expected[i]);
		}
	}

	@Test
	public void TestConvertOctal() {
		int source = 15;
		assertEquals(Convert.getOctal(source), 17);
	}

	@Test
	public void TestConvertMinus() {
		int source = -15;
		assertEquals(ConvertNumberToRuWords.convertToWords(source), "минус пятнадцать");
	}

}
