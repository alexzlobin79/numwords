package com.numinwords.project;

import static org.junit.Assert.*;
import org.junit.Test;

import com.numinwords.project.convert.ConvertNumberToEnWords;

public class TestConvertNumberInEnWords {

	// @Test
	public void TestConvertOneThreeDigits() {

		int[] source = { 0, 1, 9, 19, 22, 40, 101, 119, 145, 100, 940 };
		String[] expected = { "zero", "one", "nine", "nineteen", "twenty-two", "forty", "one hundred one",
				"one hundred nineteen", "one hundred forty-five", "one hundred", "nine hundred forty" };

		for (int i = 0; i < source.length; i++) {

			System.out.println(ConvertNumberToEnWords.convertToWords(source[i]));

		}

		for (int i = 0; i < source.length; i++) {

			// System.out.println(expected[i]);

			assertEquals(ConvertNumberToEnWords.convertToWords(source[i]), expected[i]);
		}
	}

	// @Test
	public void TestConvertFourFiveDigits() {

		int[] source = { 1101, 2119, 5121, 6452, 50000 };
		String[] expected = { "one thousand one hundred one", "two thousand one hundred nineteen",
				"five thousand one hundred twenty-one", "six thousand four hundred fifty-two", "fifty thousand" };
		for (int i = 0; i < source.length; i++) {
			assertEquals(ConvertNumberToEnWords.convertToWords(source[i]), expected[i]);
		}
	}

	@Test
	public void TestConvertBigNumber() {

		long[] source = { 55451129673430L };
		String[] expected = {
				"fifty-five trillion four hundred fifty-one billion one hundred twenty-nine million six hundred seventy-three thousand four hundred thirty" };

		for (int i = 0; i < source.length; i++) {
			assertEquals(ConvertNumberToEnWords.convertToWords(source[i]), expected[i]);
		}
	}

	@Test
	public void TestConvertMinus() {
		int source = -15;
		assertEquals(ConvertNumberToEnWords.convertToWords(source), "minus fifteen");
	}

}
